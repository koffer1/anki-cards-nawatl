# Tarjetas anki de las clases Nawatl (nahuatl)

Tarjetas que voy agregando de mis clases de nawatl de la huasteca.

sirve para ir repasando vocabulario.

Se requiere el programa [Anki](https://apps.ankiweb.net)
para poder usarlas.


Direccion del repositorio
[https://gitlab.com/koffer1/anki-cards-nawatl](https://gitlab.com/koffer1/anki-cards-nawatl)


## English translate

Nawatl anki cards (nahuatl)

Cards that I am adding from my Nawatl classes from the Huasteca region.

It is used to review vocabulary.

The Anki program is required [https://apps.ankiweb.net](https://apps.ankiweb.net)
to be able to use the cards.


Repository address

[https://gitlab.com/koffer1/anki-cards-nawatl](https://gitlab.com/koffer1/anki-cards-nawatl)

2021